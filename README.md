# How to integrate with Holm Bank Open API

Holm Open API is a RESTful API used for enabling hire-purchase offers and contracts.
We use JWT to authorize the requests and verify responses (https://jwt.io/introduction)
Every request is sent on behalf of end-customer by on-line store.

## Create keys for signing and verifying JWT requests

	openssl genrsa -out your_store_private_key 2048
	openssl rsa -in your_store_private_key -pubout > your_store_public_key

Send your public key `your_store_public_key` to Holm and get back

- Holm public key (you will need it to verify responses)
- Holm Audience ID string (AUD, Holm id)
- Issuer ID (ISS, store id)

## Use JWT to communicate with Holm Open API

For each HTTP request, create a JWT token setting the following fields:

- iss: your issuer ID string (also identifies the online store)
- aud: Holm audience ID string
- exp: set it into the future, for example current UTC time + 60 seconds
- sub: set it to "openapi-request"
- rbh: request body hash (see below how to calculate it)

Sign the JWT token with your private key using RS256 algorithm.

Set the JWT token in the request "Authorization" header as

	"Authorization": "Bearer <JWT token>"

Each response will contain a JWT token in the "Authorization" header as

	"Authorization": "Bearer <JWT token>"

For each response, it is expected that you verify the JWT token too.

## How to calculate request/response body hash

    rbh = base64encode(sha256hash(body))

Important: sha256 hash of request body should be a binary representation and not the hex output.

## Example Java source code

Please take a look at:
- CheckAccess.java
- GetHirePurchasePreliminaryFinancialConditions.java
- GetContractStatus.java
- GetCustomerEmail.java
- PostHirePurchasePartialReturn.java
- PostHirePurchaseReturn.java

## Open API endpoints

### Check Open API access

    POST /api/open/check-access
    
    BODY:
    {
    "nonce": "2023-01-01T12:45:22.9836235"
    }

    RESPONSE:
    "OK: 2023-01-01T12:45:22.9836235"

### Hire-purchase preliminary conditions

    POST /api/open/hire-purchase-preliminary-conditions
    
    BODY:
    {
    "personalCode": "38911226041",
    "amount": 5000,
    "periodInMonths": 6,
    "productCode": "PRN_HP"
    }

    POSITIVE RESPONSE:
    {
    "status": "APPROVED",
    "interestRate": 11.40,
    "contractFee": 1.00,
    "monthlyPayment": 581.53,
    "message": "Lisaks vajalik sissemakse 1709.20 eurot"
    }

    NEGATIVE RESPONSE:
    {
    "status": "REJECTED"
    }

### Get contract status by order id

    GET /order/:orderId/contract

    RESPONSE:
    {
    "status": "ACTIVE",
    "contractNumber": "67f89d42-efb3-414f-a9e9-d11b403d6cf8"
    }

### Get customer contact by order id

    GET /order/:orderId/customer

    RESPONSE:
    {
    "email": "test.email@test.ee"
    }

### Make partial purchase return

    POST /api/open/hire-purchase-partial-return
    
    BODY:
    {
    "returnedProducts": [
        {
        "productName": "Product name",
        "quantity": 1,
        "totalPrice": 144.99,
        "productCode": "XYZ123"
        }
    ]
    }

    RESPONSE:
    {
    "message": "OK"
    }

### Make full purchase return

    POST /api/open/hire-purchase-return
    
    BODY:
    {}

    RESPONSE:
    {
    "message": "OK"
    }
