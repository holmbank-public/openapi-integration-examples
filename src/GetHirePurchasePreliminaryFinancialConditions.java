import ee.holmbank.openapi.client.OpenApiHttpClient;

import java.math.BigDecimal;
import java.util.Map;

public class GetHirePurchasePreliminaryFinancialConditions {
  public static void main(String[] args) {
    OpenApiHttpClient client = new OpenApiHttpClient();

    String personalCode = "38911226041";
    BigDecimal purchaseAmount = new BigDecimal("1234.56");
    int periodLengthInMonths = 6;
    String productCode = "PRN_HP_ESHOP";

    String response = client.post("/api/open/hire-purchase-preliminary-conditions",
        OpenApiHttpClient.payload(
            Map.of(
                "personalCode", personalCode,
                "amount", purchaseAmount,
                "periodInMonths", periodLengthInMonths,
                "productCode", productCode)
        )
    );
    System.out.println(response);
  }
}
