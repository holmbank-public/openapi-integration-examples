import ee.holmbank.openapi.client.OpenApiHttpClient;

import java.util.Map;
import java.util.UUID;

public class PostHirePurchaseReturn {
    public static void main(String[] args) {
        OpenApiHttpClient client = new OpenApiHttpClient();

        String orderId = UUID.fromString("39fb1247-eaec-49c6-98fd-3e843b250b9c").toString();

        String response = client.post(String.format("/api/open/order/%s/hire-purchase-return", orderId),
                OpenApiHttpClient.payload(
                        Map.of()
                )
        );
        System.out.println(response);
    }
}
