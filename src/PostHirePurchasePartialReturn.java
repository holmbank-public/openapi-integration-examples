import ee.holmbank.openapi.client.OpenApiHttpClient;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PostHirePurchasePartialReturn {
    public static void main(String[] args) {
        OpenApiHttpClient client = new OpenApiHttpClient();

        String orderId = UUID.fromString("39fb1247-eaec-49c6-98fd-3e843b250b9c").toString();

        List<Map<String, Serializable>> returnedProducts = List.of(
                Map.of(
                        "productName", "Product name",
                        "productCode", "XYZ123",
                        "quantity", new BigDecimal("1"),
                        "totalPrice", new BigDecimal("144.99")
                )
        );

        String response = client.post(String.format("/api/open/order/%s/hire-purchase-partial-return", orderId),
                OpenApiHttpClient.payload(
                        Map.of(
                                "returnedProducts", returnedProducts)
                )
        );
        System.out.println(response);
    }
}
