import ee.holmbank.openapi.client.OpenApiHttpClient;

import java.time.LocalDateTime;
import java.util.Map;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

public class CheckAccess {
  public static void main(String[] args) {
    String response = new OpenApiHttpClient().post("/api/open/check-access",
        OpenApiHttpClient.payload(Map.of("nonce", LocalDateTime.now().format(ISO_DATE_TIME))));

    System.out.println(response);
  }
}
