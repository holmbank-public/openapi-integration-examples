import ee.holmbank.openapi.client.OpenApiHttpClient;

import java.util.UUID;

public class GetContractStatus {
    public static void main(String[] args) {
        OpenApiHttpClient client = new OpenApiHttpClient();

        String orderId = UUID.fromString("39fb1247-eaec-49c6-98fd-3e843b250b9c").toString();

        String response = client.get(String.format("/api/open/order/%s/contract", orderId));
        System.out.println(response);
    }
}
