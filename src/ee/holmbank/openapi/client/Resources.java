package ee.holmbank.openapi.client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Resources {
  public static String toString(InputStream stream) {
    try (Scanner scanner = new Scanner(stream, UTF_8)) {
      return scanner.useDelimiter("\\A").next();
    }
  }

  public static InputStream getInputStream(String classpathOrFilesystemLocation) {
    String classpathPrefix = "classpath:";
    if (classpathOrFilesystemLocation.startsWith(classpathPrefix)) {
      classpathOrFilesystemLocation = classpathOrFilesystemLocation.substring(classpathPrefix.length());
      InputStream stream = Resources.class.getResourceAsStream(classpathOrFilesystemLocation);
      if (stream == null) throw new IllegalStateException("Resource [" + classpathOrFilesystemLocation + "] not found in classpath.");
      return stream;
    }
    try {
      return new FileInputStream(classpathOrFilesystemLocation);
    } catch (FileNotFoundException e) {
      throw new UncheckedIOException("Resource [" + classpathOrFilesystemLocation + "] not found in filesystem.", e);
    }
  }
}
