package ee.holmbank.openapi.client;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class OpenApiHttpClientConfiguration {
  String baseUrl;
  String issuerId;
  String audienceId;
  String clientPrivateKeyPem;
  String holmPublicKeyPem;
}
