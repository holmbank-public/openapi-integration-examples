package ee.holmbank.openapi.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.SneakyThrows;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.function.Consumer;

import static com.nimbusds.jose.JWSAlgorithm.RS256;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.Duration.ofSeconds;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.stream.Collectors.joining;

public class OpenApiHttpClient {
  private final HttpClient httpClient = HttpClient.newBuilder().connectTimeout(Duration.of(10, SECONDS)).build();
  private final OpenApiHttpClientConfiguration configuration;
  private final Clock clock = Clock.systemDefaultZone();

  public OpenApiHttpClient() {
    this.configuration = getOpenApiHttpClientConfiguration();
  }

  public String get(String service, Map<String, String> params) {
    return execute(service + "?" + urlEncodeParams(params), HttpRequest.Builder::GET);
  }

  public String post(String service, String message) {
    return execute(service, r -> r.POST(HttpRequest.BodyPublishers.ofString(message)), message);
  }

  private String execute(String service, Consumer<HttpRequest.Builder> requestConsumer) {
    return execute(service, requestConsumer, null);
  }

  private String execute(String service, Consumer<HttpRequest.Builder> requestConsumer, String message) {
    try {
      String bodyHash = message != null ? calculateHash(message) : null;
      String jwt = createJwt(bodyHash);

      URI uri = URI.create(withoutTrailingSlash(configuration.getBaseUrl()) + service);
      HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder()
          .uri(uri)
          .header("Content-type", "application/json")
          .header("Authorization", "Bearer " + jwt)
          .timeout(ofSeconds(60));
      requestConsumer.accept(httpRequestBuilder);
      HttpRequest httpRequest = httpRequestBuilder.build();

      System.out.printf("%s %s %s%n", httpRequest.method(), uri, message != null ? message : "");
      HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

      int status = httpResponse.statusCode();
      String responseBody = httpResponse.body();

      if (status < 300) {
        String responseJwtToken = httpResponse.headers().firstValue("Authorization")
            .map(s -> s.substring(7))
            .orElseThrow(() -> new RuntimeException("no Authorization header in response"));
        SignedJWT responseJwt = SignedJWT.parse(responseJwtToken);
        verify(responseJwt);
        if (!calculateHash(responseBody).equals(responseJwt.getJWTClaimsSet().getStringClaim("rbh")))
          throw new RuntimeException("incorrect response hash");
      } else if (status < 500) {
        throw new RuntimeException(status + " " + responseBody);
      } else {
        throw new RuntimeException(status + " " + responseBody);
      }
      return responseBody;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private String createJwt(String bodyHash) throws JOSEException {
    JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder()
        .issuer(configuration.getIssuerId())
        .audience(configuration.getAudienceId())
        .expirationTime(new Date(System.currentTimeMillis() + 59000))
        .subject("openapi-request");
    if (bodyHash != null) builder.claim("rbh", bodyHash);
    JWTClaimsSet claimsSet = builder.build();

    String privateKey = Resources.toString(Resources.getInputStream(configuration.getClientPrivateKeyPem()));
    RSAKey rsaKey = JWK.parseFromPEMEncodedObjects(privateKey).toRSAKey();
    JWSSigner signer = new RSASSASigner(rsaKey.toPrivateKey());

    SignedJWT signedJWT = new SignedJWT(new JWSHeader(RS256), claimsSet);
    signedJWT.sign(signer);

    return signedJWT.serialize();
  }

  private static String calculateHash(String message) {
    try {
      return Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(message.getBytes(UTF_8)));
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  private void verify(SignedJWT signedJWT) throws ParseException, JOSEException {
    String openApiPublicKey = Resources.toString(Resources.getInputStream(configuration.getHolmPublicKeyPem()));
    RSAKey rsaKey = JWK.parseFromPEMEncodedObjects(openApiPublicKey).toRSAKey();
    if (!signedJWT.verify(new RSASSAVerifier(rsaKey)))
      throw new RuntimeException("not verified");

    JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();
    Instant notBeforeTime = claimsSet.getNotBeforeTime().toInstant();
    Instant expirationTime = claimsSet.getExpirationTime().toInstant();
    verifyTimeValidity(Instant.now(clock), notBeforeTime, expirationTime);
  }

  void verifyTimeValidity(Instant currentTime, Instant notBeforeTime, Instant expirationTime) {
    int timeDifferenceToleranceInSeconds = 3;

    if (expirationTime.isBefore(currentTime.minusSeconds(timeDifferenceToleranceInSeconds)))
      throw new RuntimeException("token expired");

    if (currentTime.plusSeconds(timeDifferenceToleranceInSeconds).isBefore(notBeforeTime))
      throw new RuntimeException("token expired");
  }

  public static String payload(Map<String, Object> payload) {
    var mapper = new ObjectMapper() {
      @SneakyThrows
      public String render(Object body) {
        return writeValueAsString(body);
      }
    };
    return mapper.render(payload);
  }

  private static String urlEncodeParams(Map<String, String> params) {
    return params.entrySet().stream()
        .map(param -> param.getKey() + "=" + URLEncoder.encode(param.getValue(), UTF_8))
        .collect(joining("&"));
  }

  private static String withoutTrailingSlash(String value) {
    if (value == null) return null;
    if (!value.endsWith("/")) return value;
    return value.substring(0, value.length() - 1);
  }

  private static OpenApiHttpClientConfiguration getOpenApiHttpClientConfiguration() {
    return OpenApiHttpClientConfiguration.builder()
        .baseUrl("http://localhost:4568")
        .issuerId("daf6214c-7473-4b8a-bbd8-f41d8f3bed07")
        .audienceId("=ffZ7$4(Pum2q?Ub9pum7gx]SHOj*tK4y<tBvL#p")
        .clientPrivateKeyPem("classpath:/openapi-client-private.pem")
        .holmPublicKeyPem("classpath:/holm-public.pem")
        .build();
  }
}
